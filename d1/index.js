
let studentsAverage = [98.5,94.3,89.2,90.1];
console.log(studentsAverage);

let computerBrands = ["Lenovo","Acer","MSI","HP","Asus","Dell","Cdr-King"];
console.log(computerBrands);
let mixedArr = [12, "Asus", null, undefined];
console.log(mixedArr);
console.log("Array before reassigning: ");
console.log(mixedArr);
mixedArr[0] = "Hello world!";
console.log("Array after reassigning: ");
console.log(mixedArr);

//Section = reading from arrays

console.log(studentsAverage);
console.log(studentsAverage[0]);
console.log(computerBrands[3]);
console.log(computerBrands[6]);

if (computerBrands.length>5){
	console.log("We have too many suppliers");
}

let lastElementIndex = computerBrands.length-1;
console.log(computerBrands[lastElementIndex]);

//Section - Array methods
// methods are similar to functions, these array methods can be done in array and objects alone
//Mutator Methods

let fruits = ["Apple","Mango","Rambutan","Lanzones","Durian"];
console.log(fruits);
//push() - 
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);

console.log("Mutated Array from Push Method");
console.log(fruits);

// fruits[6] = "Orange";
// console.log(fruits);
fruits.push("Orange", "Avocado");
console.log("Mutated Array from Push Method:");
console.log(fruits);

//pop() - remove an element from the end of an array
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated Array from pop method");
console.log(fruits);

// unshift - adds an element at the start of array

fruits.unshift("Lime","Banana");
console.log("Mutated Array from unshift method");
console.log(fruits);

// shift - works like pop but this will remove the element at the start of the array

let fruitsRemoved = fruits.shift();
console.log("Mutated Array from shift method");
console.log(fruits);

//splice - simultaneously remove

fruits.splice(1,2, "Lime","Cherry");
console.log("Mutated Array from Splice method");
console.log(fruits);

// sort = rearranges the elements in an array in alphanumeric order

fruits.sort();
console.log("Mutated Array from Sort method");
console.log(fruits);

//reverse - reverses the order of the array elements

fruits.reverse();
console.log("Mutated Array from Revers method");
console.log(fruits);

// Non-mutator Methods
// these are functions that do not modify the original array. Also, these methods do not manipulate the elements inside the array even if they are performing tasks such as returning elements from an array and combining them with other arrays and printing the output.

//indexOf();
// returns the index of the first matching element found in an array
// the search process will start from the first element down to the last.

let countries = ["RUS","CH","JPN","PH","USA","KOR","AUS","CAN","PH"];
let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: "+firstIndex);


// lastIndexOf();
// this search the process from the last element down to first.
let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf: "+lastIndex);

//slice
// copies a part of the array and returns a new array.
let slicedArrayA = countries.slice(2);
console.log("Result of Slice: "+slicedArrayA);
console.log(countries);
// slice syntax slice(startingIndex of input and up to the last index);
let slicedArrayB = countries.slice(2,4);
console.log("Result of Slice: "+slicedArrayB);
console.log(countries);
// - numbers will start slicing off the elements from the end of the array.
let slicedArrayC = countries.slice(-3);
console.log("Result of Slice: "+slicedArrayC);
console.log(countries);

//toString();
let stringArray = countries.toString();
console.log("Result of toString: ");
console.log(stringArray);

//concat - used to combine two arrays and returns the combined result
let tasksA = ["drink HTML", "eat Javascript"];
let tasksB = ["inhale CSS", "breathe SASS"];
let tasksC = ["get Git","be node"];

let tasks = tasksA.concat(tasksB);
console.log("Result of concat: ");
console.log(tasks)

// combining multiple arrays

let allTasks = tasksA.concat(tasksB, tasksC);
console.log("Result of concat: ");
console.log(allTasks)

//join method
// returns an array as string seperated by specified string separator

let users = ["John","Jane","Joe","Jobert","Julius"];

console.log(users.join());
console.log(users.join(' '));
console.log(users.join('-'));

// iterator methods
// iteration methods are loops designed to perform repetitive tasks on arrays
// useful in manipulating array data resulting in complex tasks

//forEach

allTasks.forEach(function(task){
	console.log(task);
});

let filteredTasks = [];

allTasks.forEach(function(task){
	if(task.length >10){
		//we stored the filtered elements inside another variable to avoid confusion should we need the original array in tact
		filteredTasks.push(task);
	};
});

console.log("The result of forEach: ");
console.log(filteredTasks);

// iterates on each element and returns a new array with different values depending on the result of the function's operation.
// this is useful for performing tasks where mutating/changing the elements are required

let numbers = [1,2,3,4,5];

let numbersMap = numbers.map(function(number){
	//unlike forEach - since this has a function. return statement is needed to create a value that is stored in another array.
	return number * number;
});
console.log("The result of map:");
console.log(numbersMap);

//Syntax: let/const arrayName = arrayName.map(function(individual element){ return statement})

//every()
/*
	- checks if all elements pass the given condition
	returns a boolean data type depending if all elements meet the condition.
*/

let allValid = numbers.every(function(number){
	return (number<3);
});
console.log("Result of every: ");
console.log(allValid);
console.log(numbers);

// some()
/*
	this checks if atleast one element passes the condition in the array
	returns boolean data type
*/

let someValid = numbers.some(function(number){
	return (number<2);
});
console.log("Result of some: ");
console.log(someValid);
console.log(numbers);

//filter
// returns a new array that contain copies of the elements which meet the given condition
// returns empty array if there are no elements that meet that condition
// useful for the filtering array elements with a given and shortens the syntax compared to using other array iteration method such as forEach + if statement + push in an earlier example.

let filterValid = numbers.filter(function(number){
	return(number<3);
});
console.log("Result of filter: ");
console.log(filterValid);
console.log(numbers);

