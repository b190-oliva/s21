
let cities = ["Tokyo","Manila","Seoul","Jakarta","Sim"];
console.log(cities);
console.log(cities.length);
console.log(cities.length-1);
console.log(cities[cities.length-1]);

blankArr = [];
console.log(blankArr);
console.log(blankArr.length);
console.log(blankArr.length[blankArr.length]);

console.log(cities);
cities.length--; //decrement will delete the last element of the array.
console.log(cities);

let lakersLegends = ["Kobe", "Shaq", "Magic", "Kareem", "lebRon"];
console.log(lakersLegends);
lakersLegends.length++;
console.log(lakersLegends);
lakersLegends[5] = "Pau Gasol";
console.log(lakersLegends);

lakersLegends[lakersLegends.length] = "Anthony Davis";
console.log(lakersLegends);

let numArray = [5,12,30,46,40];

for(let index =0; index < numArray.length; index++){
		if(numArray[index] % 5 ===0){
				console.log(numArray[index]+" is divisible by 5");
		}
		else{
			console.log(numArray[index]+" is not divisible by 5");
		}
}

for(index = 0; index < numArray.length; index++){
		console.log(numArray[index]);
}

for(index = numArray.length-1; index >= 0 ; index--){
		console.log(numArray[index]);
}

// multidimensional array

let chessBoard = [
["a1","b1","c1","d1","e1"],
["a2","b2","c2","d2","e2"],
["a3","b3","c3","d3","e3"],
["a4","b4","c4","d4","e4"],
["a5","b5","c5","d5","e5"],
];
console.log(chessBoard);
// array multidimensional accessing 
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " +chessBoard[2][3]);